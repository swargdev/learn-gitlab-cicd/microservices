# Microservices project
one frontend service
two backend services:
 - products
 - shopping-cart

## Start the services locally (repeat for each micro service)
    cd <micro-service-name>
    npm install && npm run

## Build
    docker build -t ms-frontend:1.0 frontend
    docker build -t ms-products:1.0 products
    docker build -t ms-shopping-cart:1.0 shopping-cart

## Start as Docker Containers - separate commands
    docker run -d -p 8000:8000 \
    -e PRODUCTS_SERVICE=host.docker.internal \
    -e SHOPPING_CART_SERVICE=host.docker.internal \
    ms-frontend:1.0

    docker run -d -p 8001:8001 ms-products:1.0
    docker run -d -p 8002:8002 ms-shopping-cart:1.0

## Start with Docker-Compose (repeat for each micro service)
    cd <micro-service-name>

    export COMPOSE_PROJECT_NAME=micro-service-name (frontend)
    export DC_IMAGE_NAME=micro-service-image-name (ms-frontend)
    export DC_IMAGE_TAG=tag (1.0)
    export DC_APP_PORT=port (8000)

    docker-compose -d up

