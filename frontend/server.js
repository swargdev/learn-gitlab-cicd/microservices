let express = require('express')
let path = require('path')
let fs = require('fs')
let app = express()

// with docker-compose: container-name, with K8s: service-name
let productsEndpoint = process.env.PRODUCTS_SERVICE || 'localhost'
let shoppingCartEndpoint = process.env.SHOPPING_CART_SERVICE || 'localhost'

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'index.html'))
})

app.get('/img/products.jpg', function (req, res) {
  let img = fs.readFileSync(path.join(__dirname, 'images/products.jpg'))
  res.writeHead(200, { 'Content-Type': 'image/jpg' })
  res.end(img, 'binary')
})

app.get('/img/shopping-cart.jpg', function (req, res) {
  let img = fs.readFileSync(path.join(__dirname, 'images/shopping-cart.jpg'))
  res.writeHead(200, { 'Content-Type': 'image/jpg' })
  res.end(img, 'binary')
})

app.get('/get-products', function (req, res) {
  var http = require('http')

  var options = {
    host: productsEndpoint,
    path: '/',
    port: '8001',
    method: 'GET',
  }

  callback = function (response) {
    var str = ''
    response.on('data', function (chunk) {
      str += chunk
    })

    response.on('end', function () {
      console.log(str)
      res.writeHead(200)
      res.end(str)
    })
  }

  var req = http.request(options, callback)
  // This is a request to "products" service
  req.write('')
  req.end()
})

app.get('/get-shopping-cart', function (req, res) {
  var http = require('http')

  var options = {
    host: shoppingCartEndpoint,
    path: '/',
    port: '8002',
    method: 'GET',
  }

  callback = function (response) {
    var str = ''
    response.on('data', function (chunk) {
      str += chunk
    })

    response.on('end', function () {
      console.log(str)
      res.writeHead(200)
      res.end(str)
    })
  }

  var req = http.request(options, callback)
  // This is a request to "shopping-cart" service
  req.write('')
  req.end()
})

app.listen(8000, function () {
  console.log('app listening on port 8000')
})


